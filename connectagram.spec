Name:           connectagram
Version:        1.3.1
Release:        1%{?dist}
Summary:        Linked anagram game

License:        GPLv3+
URL:            http://gottcode.org/%{name}/
Source:         http://gottcode.org/%{name}/%{name}-%{version}-src.tar.bz2

BuildRequires:  desktop-file-utils
BuildRequires:  libappstream-glib
BuildRequires:  qt5-qttools-devel

Requires:       hicolor-icon-theme

%description
Connectagram is a word unscrambling game. The board consists of several
scrambled words that are joined together. You can choose the length of the
words, the amount of words, and the pattern that the words are arranged in.
The game provides a hint option for times when you are stuck, and features
an online word lookup that fetches the definitions of each word from Wiktionary.


%prep
%setup -q


%build
%{qmake_qt5} PREFIX=%{_prefix}
make %{?_smp_mflags}


%install
%make_install INSTALL_ROOT=%{buildroot}

%find_lang %{name} --with-qt --without-mo

%check
desktop-file-validate %{buildroot}/%{_datadir}/applications/%{name}.desktop || :
appstream-util validate-relax --nonet %{buildroot}%{_datadir}/metainfo/%{name}.appdata.xml || :

%files -f %{name}.lang
%doc ChangeLog CREDITS README
%license COPYING
%{_bindir}/%{name}
%{_datadir}/metainfo/%{name}.appdata.xml
%{_datadir}/applications/%{name}.desktop
%{_datadir}/icons/hicolor/*/apps/%{name}.*
%{_datadir}/pixmaps/%{name}.xpm
%dir %{_datadir}/%{name}/
%{_datadir}/%{name}/data/
%dir %{_datadir}/%{name}/translations/
%{_mandir}/man6/%{name}.6.*

%changelog
* Mon Jun 14 2021 Yaakov Selkowitz <yselkowi@redhat.com> - 1.3.1-1
- new version

* Fri Jul 31 2020 Yaakov Selkowitz <yselkowi@redhat.com> - 1.2.11-1
- Initial package
